define(['engine', 'game_screen'], function(Engine, gameScreen, undefined) {

	window.addEventListener("click", startGameMenu, false);
	function startGameMenu(e) {
		// change to Engine.nextScene()
		Engine.currentScene = gameScreen;
		Engine.inGame = true;
		window.removeEventListener("click", startGameMenu, false);
	}

	function welcomeText() {
		Engine.setFont(32);
		Engine.ctx.fillText("Orbital Defense", -120, 200);
		Engine.setFont(20);
		Engine.ctx.fillText("Click anywhere to start", -120, 240);
	}

    var points = [], // Vertices of the 3D thingy
        size = 12, // Length (in segments) of the 3D thingy
        offsetDown = 400 // For greater view angle

    function createVertices() {
        var i, j, ii, jj, end = -size * 10 - 5
        // "top" side
        for (i = -size; ii = 10 * i, i <= size; ++i) {
            for (j = -size; jj = 10 * j, j <= size; ++j) {
                points.push(ii - 5, offsetDown, jj - 5);
                points.push(ii - 5, offsetDown, jj + 5);
                points.push(ii + 5, offsetDown, jj + 5);
                points.push(ii + 5, offsetDown, jj - 5);
                points.push(void 0, void 0, void 0); // to close path
            }
        }
        // "right" side
        for (i = -size; ii = 10 * i, i <= size; ++i) {
            for (j = -1; jj = 10 * j, j <= 1; ++j) {
                points.push(ii - 5, offsetDown - end/12 + jj, end);
                points.push(ii - 5, offsetDown - end/12 + jj + 10, end);
                points.push(ii + 5, offsetDown - end/12 + jj + 10, end);
                points.push(ii + 5, offsetDown - end/12 + jj, end);
                points.push(void 0, void 0, void 0); // to close path
            }
        }
        // "left" side
        for (i = -1; ii = 10 * i, i <= 1; ++i) {
            for (j = -size; jj = 10 * j, j <= size; ++j) {
                points.push(end, offsetDown - end/12 + ii, jj - 5);
                points.push(end, offsetDown - end/12 + ii + 10, jj - 5);
                points.push(end, offsetDown - end/12 + ii + 10, jj + 5);
                points.push(end, offsetDown - end/12 + ii, jj + 5);
                points.push(void 0, void 0, void 0); // to close path
            }
        }
    }
    createVertices(); // Should be computed just once

    function renderThingy(closePath){
        for (var i = 0; i < points.length; i += 3) {
            if (points[i] === undefined) closePath();
            else lineTo3D(points[i], points[i+1], points[i+2]);
        }
    }

    function update() {
    }

    function draw() {
        var c = Engine.ctx;
        Engine.clear();
        Engine.ctx.save();
        Engine.ctx.translate(c.canvas.width/2, c.canvas.height/2 - offsetDown);
        Engine.ctx.strokeStyle = '#8cffff';
        Engine.ctx.beginPath()
        renderThingy(function() {
            Engine.ctx.closePath()
            Engine.ctx.stroke()
            Engine.ctx.beginPath()
        });
        welcomeText();
        Engine.ctx.restore();
    }

    /* Utils for three-dimensional projection */
    var _f = 500, // Focal length, affects basically everything
        _rotation = Math.PI / 6, // Rotate the scene by about 30°
        _rcos = Math.cos(_rotation), _rsin = Math.sin(_rotation)
    function _perspectiveCorrection(z) { return _f / (_f + z) }
    /* Public-ish "API" of some sort */
    function lineTo3D(x, y, z) {
        var k
        x = x * _rcos - z * _rsin
        z = x * _rsin + z * _rcos
        k = _perspectiveCorrection(z)
        Engine.ctx.lineTo(k * x, k * y)
    }

	return {
        update: update,
        draw: draw
	}
})
