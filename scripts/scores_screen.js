define(['engine', 'start_screen'], function(Engine, startScreen) {
	var restart = false;
	if (Engine.scoring) {
		console.debug('scoring');
		window.addEventListener("click", endGameMenu, false);
	}
	function endGameMenu(e) {
		console.debug('game over');
		// change to Engine.nextScene()
		Engine.currentScene = startScreen;
		window.removeEventListener("click", endGameMenu, false);
	}

	function welcomeText() {
		Engine.setFont(20);
		Engine.ctx.fillText("Game over",
							250,
							Engine.canvas.height / 2);
		Engine.ctx.fillText("Your score is: " + Engine.scoring, 
							250, Engine.canvas.height / 2 + 40);
		Engine.ctx.fillText("refresh to restart", 
							250, Engine.canvas.height / 2 + 80);
	}

	return {
		update: function() {
			Engine.clear();
			welcomeText();
		},
		draw: function() {

		}
	}
})
