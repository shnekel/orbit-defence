define(['engine'], function(Engine) {
	function Bullet(x, y, dirX, dirY) {
		this.x = x;
		this.y = y;
		this.dirX = dirX;
		this.dirY = dirY;
		this.chx = x;
		this.chy = y;
		this.velX = 0;
		this.velY = 0;
		this.speed = 5;
		this.width = this.height = 5;
		this.active = true;
	}

	Bullet.prototype.draw = function(ctx) {
		ctx = ctx || Engine.ctx;
		ctx.fillStyle = "#ff0";
		ctx.fillRect(this.x, this.y, 5, 5);
	}

	Bullet.prototype.move = function() {
		var dx = (this.dirX - this.chx);
		var dy = (this.dirY - this.chy);
		var mag = Math.sqrt(dx*dx + dy*dy);
		this.velX = (dx / mag) * this.speed;
		this.velY = (dy / mag) * this.speed;
		this.x += this.velX;
		this.y += this.velY;
	}
	
	return Bullet;
});
