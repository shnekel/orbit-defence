define(['engine', 'block'], function(Engine, Block) {
	function Planet(x, y, size) {
		this.size = size || 10;
		this.blockSize = 20;
		this.x = x || Engine.canvas.width / 2 - this.size / 2 * this.blockSize;
		this.y = y || Engine.canvas.height / 2 - this.size / 2 * this.blockSize;
		this.height = this.width = this.size * this.blockSize;
		this.surface = [];
		this.init();
	}
	
	Planet.prototype.init = function() {
		for (var i=0; i<this.size; i++) {
			this.surface[i] = [];
			for (var j=0; j<this.size; j++) {
				this.surface[i][j] = new Block(this.x+i*this.blockSize, this.y+j*this.blockSize);
			}
		}
	}
	
	Planet.prototype.draw = function(ctx) {
		ctx = ctx || Engine.ctx;
		for (var i=this.surface.length; i--;) {
			for (var j=this.surface[i].length; j--;) {
				this.surface[i][j].draw(ctx);
			}
		}
	}

	Planet.prototype.getConditions = function() {
		var dead = 0, alive = 0;
		for (var i = 0; i<this.surface.length;i++) {
			for (var j=0; j<this.surface[i].length;j++) {
				this.surface[i][j].isDead ? dead++ : alive++;
			}
		}
		return alive;
	}
	
	return Planet;
});
