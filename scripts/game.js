define([
	'engine',
	"start_screen",
], function(Engine, startScreen) {
	var AudioContext =
			window.AudioContext ||
			window.webkitAudioContext;
	// var requestAnimationFrame =
	// 		window.requestAnimationFrame ||
	// 		window.mozRequestAnimationFrame ||
	// 		window.webkitRequestAnimationFrame ||
	// 		window.msRequestAnimationFrame ||
	// 		function(f){ setTimeout(f, 16); };

    if (typeof Date.now != 'function')
        Date.now = function() { return (new Date).getTime() }

	Engine.currentScene = startScreen;

	function gameLoop() {
        var timeLast = Date.now(), timeNow, timeDif;

		function loop() {
			Engine.request = requestAnimFrame(loop);

            timeDif = (timeNow = Date.now()) - timeLast;

            Engine.currentScene.update(timeDif, timeNow);
			Engine.currentScene.draw(timeDif, timeNow);

            timeLast = timeNow;
		}
		loop();
	}

	return {
		loop: gameLoop
	}
})







