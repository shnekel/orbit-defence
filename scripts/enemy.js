define(['engine'], function(Engine) {
	function Enemy() {
		this.reset();
		this.dirX = Engine.canvas.width/2;
		this.dirY = Engine.canvas.height/2;
		this.width = 20;
		this.height = 20;
	}

	Enemy.prototype.reset = function() {
		var direction = Engine.rnd(0, 3);
		var x, y;
		switch(direction) {
		case 0: // top
			x = Engine.rnd(0, Engine.canvas.width);
			y = 0;
			break
		case 1: // right
			x = Engine.canvas.width;
			y = Engine.rnd(0, Engine.canvas.height);
			break;
		case 2: // bottom
			x = Engine.rnd(0, Engine.canvas.width);
			y = Engine.canvas.height;
			break;
		case 3: // left
			x = 0;
			y = Engine.rnd(0, Engine.canvas.height);
		}
		this.x = x;
		this.y = y;
		this.velX = 0;
		this.velY = 0;
		this.isDead = false;
		this.speed = Engine.rnd(0, 5) / 10;
	}

	Enemy.prototype.draw = function(ctx) {
		if (this.isDead) {
			return;
		}
		ctx = ctx || Engine.ctx;
		ctx.fillStyle = "#f00";
		ctx.fillRect(this.x, this.y, this.width, this.height);
	}

	Enemy.prototype.move = function() {
		var dx = (this.dirX - this.x);
		var dy = (this.dirY - this.y);
		var mag = Math.sqrt(dx*dx + dy*dy);
		this.velX = (dx / mag) * this.speed;
		this.velY = (dy / mag) * this.speed;
		this.x += this.velX;
		this.y += this.velY;
	}

	return Enemy;
	
});
