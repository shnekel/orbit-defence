define(["engine"], function(Engine) {
	function Block(x, y, size) {
		this.x = x;
		this.y = y;
		this.size = size || 20;
		this.height = this.width = this.size;
		this.isDead = false;
		this.friendly = 1;
	}

	Block.prototype.setDead = function() {
		this.isDead = true;
		this.x = 0;
		this.y = 0;
		this.width = 0;
		this.height = 0;
	}

	Block.prototype.setFriendly = function() {
		if (this.friendly == 0) {
			this.setDead();
		} else {
			this.friendly--;
		}
	}

	Block.prototype.draw = function(ctx) {
		if (this.isDead) {
			return;
		}
		ctx = ctx || Engine.ctx;
		ctx.fillStyle = "#fff";
		if (this.friendly == 1) 
			ctx.fillRect(this.x, this.y, this.width, this.height);
		else 
			ctx.fillRect(this.x + 5, this.y + 5, this.width - 10, this.height - 10);
		
	}

	return Block;
})
