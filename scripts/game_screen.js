define([
	'engine',
	'scores_screen',
	'planet',
	'vessel',
	'enemy'
], function(Engine, scoresScreen, Planet, Vessel, Enemy) {
	var mx, my;
	var planet, vessel, enemies = [];
	var scores = 0;
	var wave = 0;
	var waveSize = 10;
	var lastWave = waveSize;
	var lives = 3;
	var stars = [];
	var gameStart = false; 
	var gameOver = false;

	function inGameEvents(e) {
		var r = Engine.canvas.getBoundingClientRect();
		mx = e.clientX - r.left;
		my = e.clientY - r.top;
		if (gameOver) {
			window.localStorage.setItem('orbitScore', scores)
			window.location = "scores.html";
			return;
		}
		if (gameStart) {
			vessel.shoot(mx, my);
		}
	}

	function reinit() {
		vessel = new Vessel(Engine.canvas.width / 2,
							Engine.canvas.height / 2);
		for (var i=0; i<waveSize; i++) {
			enemies.push(new Enemy());
		}
		for (var i=0; i<500; i++) {
			var x = Engine.rnd(0, Engine.canvas.width);
			var y = Engine.rnd(0, Engine.canvas.height);
			stars.push({x: x, y: y, opacity: Math.random()});
		}
	}

	function init() {
		planet = new Planet();
		reinit();
	}

	function drawScores() {
		Engine.setFont(24)
		Engine.ctx.fillText("Lives: " + lives, 10, 40)
		Engine.ctx.fillText("Score: " + scores, 10, 80)
		Engine.ctx.fillText("Planet condition: " +
							planet.getConditions(), 10, 120)
		Engine.ctx.fillText("Wave " + waveSize, 10, 160)
	}

	function checkCollisions() {
		// enemies vs vessel
 		for (var i=0; i<enemies.length; i++) {
			if (Engine.collides(enemies[i], vessel)) {
 				lives--;
				enemies.splice(i, 1);
				waveSize--;
			}
		}
		for (var i=0; i<planet.surface.length; i++) {
			for (var j=0; j<planet.surface[i].length; j++) {
				var b = planet.surface[i][j];
				// enemies vs planet
				for (var k=0; k<enemies.length;k++) {
					if (Engine.collides(b, enemies[k])) {
						enemies.splice(k, 1);
						waveSize--;
						b.setDead();
					}
				}
				// bullets vs planet
				for (var l=0; l<vessel.bullets.length;l++) {
					if (!vessel.bullets[l].active) {
						continue;
					}
					if (Engine.collides(b, vessel.bullets[l])) {
						vessel.bullets[l].active = false;
						vessel.bullets.splice(l, 1);
						b.setFriendly();
					}
				}
			}
		}
		// bullets vs enemies
		for (var i=0; i<vessel.bullets.length; i++) {
			var b = vessel.bullets[i];
			if (!b.active) {
				continue;
			}
			for (var j=0; j<enemies.length; j++) {
				if (Engine.collides(b, enemies[j])) {
					enemies.splice(j, 1);
					waveSize--;
					scores += 10;
					b.active = false;
				}
			}
		}
	}
	function drawBackground() {
		for (var i=0; i<500; i++) {
			Engine.ctx.fillStyle = "rgba(255, 255, 255, "+stars[i].opacity+")";
			Engine.ctx.fillRect(stars[i].x, stars[i].y, 1, 1);
		}
	}

	function setGameOver() {
		Engine.inGame = false;
		Engine.scoring = scores;
		cancelRequestAnimFrame(Engine.request);
		Engine.ctx.fillStyle = "#000";
		Engine.ctx.strokeStyle = "#fff";
		var cX = Engine.canvas.width / 2;
		var cY = Engine.canvas.height / 2;
		Engine.ctx.fillRect(cX - 200, cY - 100, 400, 150);
		Engine.ctx.strokeRect(cX - 200, cY - 100, 400, 150);
		Engine.setFont(20);
		Engine.ctx.fillStyle = "#fff";
		Engine.ctx.fillText('Game over', cX - 180, cY - 70);
		Engine.ctx.fillText('Your score is ' + Engine.scoring, cX - 180, cY - 40);
		Engine.ctx.fillText('Click to see high scores', cX - 180, cY - 10);
		gameOver = true;
	}

	init();
	window.addEventListener("click", inGameEvents, false);
	return {
		update: function() {
			gameStart = true;
			if (waveSize == 0) {
				waveSize = lastWave + 5;
				lastWave = waveSize;
				reinit();
				for (var i = 0; i < enemies.length; i++) {
					enemies[i].speed += 0.1;
				}
			}
			vessel.move();
			for (var i = 0; i < enemies.length; i++) {
				enemies[i].move();
			}
			checkCollisions();
		},
		draw: function() {
			Engine.clear();
			drawBackground();
			planet.draw();
			vessel.draw();
			for (var i = 0; i < enemies.length; i++) {
				enemies[i].draw();
			}
			drawScores();
			if (planet.getConditions() == 0 ||
				lives == 0) {
				setGameOver();
			}
		}
	}
})
