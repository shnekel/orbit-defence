define([], function() {
	var canvas = document.getElementsByTagName('canvas')[0];
	canvas.style.width = (canvas.width = innerWidth) + 'px';
	canvas.style.height = (canvas.height = innerHeight) + 'px';
	var ctx = canvas.getContext("2d");

	var currentScene;
	var inGame = false;
	var scenes = [];

	window.requestAnimFrame = (function(){
		return  window.requestAnimationFrame       || 
			window.webkitRequestAnimationFrame || 
			window.mozRequestAnimationFrame    || 
			window.oRequestAnimationFrame      || 
			window.msRequestAnimationFrame     || 
			function(/* function */ callback, /* DOMElement */ element){
				return window.setTimeout(callback, 1000 / 60);
			};
	})();
	
	window.cancelRequestAnimFrame = ( function() {
		return window.cancelAnimationFrame          ||
			window.webkitCancelRequestAnimationFrame    ||
			window.mozCancelRequestAnimationFrame       ||
			window.oCancelRequestAnimationFrame     ||
			window.msCancelRequestAnimationFrame        ||
			clearTimeout
	} )();
	
	function clear(color) {
		ctx.fillStyle = color || "#000";
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	}

	// result is radians
	// to convert to degrees - * 180 / Math.PI
	function findAngle(p0,p1,p2) {
		var b = Math.pow(p1.x-p0.x,2) + Math.pow(p1.y-p0.y,2),
			a = Math.pow(p1.x-p2.x,2) + Math.pow(p1.y-p2.y,2),
			c = Math.pow(p2.x-p0.x,2) + Math.pow(p2.y-p0.y,2);
		return Math.acos( (a+b-c) / Math.sqrt(4*a*b) );
	}

	function rnd(min, max) {
		return ~~(Math.random() * (max - min + 1)) + min
	}

	function collides(a, b) {
		return a.x < b.x + b.width &&
			a.x + a.width > b.x &&
			a.y < b.y + b.height &&
			a.y + a.height > b.y;
	}

    function setFont(size, color) {
        ctx.font = (size || 32) + 'px Consolas, Monaco, monospace';
        ctx.fillStyle = color || '#fff';
    }

	return {
		canvas: canvas,
		ctx: ctx,
		clear: clear,
		rnd: rnd,
		collides: collides,
        setFont: setFont,
		currentScene: currentScene,
		inGame: inGame,
		findAngle: findAngle
	}
})
