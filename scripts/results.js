require.config({
	paths: {
		app: 'app',
		jquery: '../bower_components/jquery/dist/jquery',
		underscore: '../bower_components/underscore/underscore',
		backbone: '../bower_components/backbone/backbone',
		text: '../bower_components/requirejs-text/text'
	},

	shim: {
		'backbone': {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		},
		'jquery': {
			exports: '$'
		},
		'underscore': {
			exports: '_'
		}
	}
})

require(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
	var ResultModel = Backbone.Model.extend({
		url: "/api/scores",
		parse: function(res) {
			this.set("results", res);
		}
	});
	
	var ResultsView = Backbone.View.extend({
		el: "#otherScore",
		template: _.template($("#tableTemplate").html()),
		initialize: function(options) {
			this.model = new ResultModel();
			this.listenTo(this.model, "change", this.render);
			this.model.fetch();
		},
		render: function() {
			this.$el.html(this.template({data: this.model.get("results")}));
			return this;
		}
	})
	
	var score = window.localStorage.getItem('orbitScore');
	var resultsView = new ResultsView();
	if (score) {
		$("#yourScore").show();
        $("#addScore").click(function() {
            $.ajax({
                type: 'POST',
                data: {
                    "user": $("#playerName").val(),
                    "email": "random@" + score,
                    "result": score
                },
                success: function() {
					resultsView.model.fetch();
                },
                error: function(){},
                url: '/api/scores',
                cache:false
            });
			$("#yourScore").hide();
        });
        window.localStorage.clear();
	} 
	
});
