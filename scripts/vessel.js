define(['engine', 'bullet'], function(Engine, Bullet) {
	function Vessel(x, y) {
		this.x1 = x;
		this.y1 = y;
		this.velX = 0;
		this.velY = 0;
		this.speed = 1;
		this.radius = 200;
		this.bullets = [];
		this.width = this.height = 5;
	}
	
	Vessel.prototype.draw = function(ctx) {
		ctx = ctx || Engine.ctx;
		ctx.strokeStyle = "#333";
		ctx.beginPath();
		ctx.arc(this.x1, this.y1, this.radius, 0, Math.PI*2, false);
		ctx.stroke();
		ctx.fillStyle = "#fff";
		ctx.fillRect(this.x1 + this.velX, this.y1 + this.velY, 5, 5);

		for (var i=0; i<this.bullets.length;i++) {
			if (!this.bullets[i].active) {
				continue;
			}
			if (this.bullets[i].x > Engine.canvas.width || 
				this.bullets[i].x < 0) {
				continue;
			}
			if (this.bullets[i].y > Engine.canvas.height || 
				this.bullets[i].y < 0) {
				continue;
			}
			this.bullets[i].move();
			this.bullets[i].draw();
		}

	}
	
	Vessel.prototype.move = function() {
		this.velX = Math.cos(this.speed) * this.radius;
		this.velY = Math.sin(this.speed) * this.radius;
		this.speed += 0.01;
		this.x = this.x1 + this.velX;
		this.y = this.y1 + this.velY;
	}

	Vessel.prototype.shoot = function(dirx, diry) {
		var angle = Engine.findAngle(
			{x:this.x1, y: this.y1},
			{x:this.x1 + this.velX, y: this.y1 + this.velY},
			{x:dirx, y: diry});
		if (angle * 180 / Math.PI > 90) {
			this.radius--;
		} else {
			this.radius++;
		}
		this.bullets.push(new Bullet(this.x1 + this.velX, this.y1 + this.velY, dirx, diry));
	}

	return Vessel;
});
